import tkinter
import tkinter.messagebox
import subprocess
import os

WINDOW_WIDTH = 600
WINDOW_HEIGHT = 780

def confirm(title, message):
    target = appListbox.get(tkinter.ACTIVE)

    if tkinter.messagebox.askokcancel(title, message, icon='warning'):
        if os.system('adb shell "pm uninstall -k --user 0 %s"' % (target)):
            tkinter.messagebox.showinfo('Success', '%s has been successfully removed' % (target))
        else:
            tkinter.messagebox.showerror('Failed', '%s could not be removed' % (target))

def showHowToEnableDeveloperMode():
    tkinter.messagebox.showinfo('How to connect to phone?', '''1. Enabling developer mode
                                                                 - Go to `Settings > About device`
                                                                 - Click `Build number` 7 times
                                                            2. Enabling USB debugging
                                                                 - Go to `Developer options(mode)`
                                                                 - Enable `USB debugging`
                                                            3. Connecting to PC
                                                                 - Connect phone to pc with cable
                                                                 - Restart this program
                                                                 - If a dialog is shown in your phone, choose `Allow`
                                                                 - Restart this program
                                                            ''')

root = tkinter.Tk()

root.title('Preinstalled Remover')

root.geometry("%dx%d" % (WINDOW_WIDTH, WINDOW_HEIGHT))
root.resizable(False, False)

helpButton = tkinter.Button(root, text="How to connect to phone?", height=1, command=showHowToEnableDeveloperMode)
helpButton.pack(fill='x')

appListbox = tkinter.Listbox(root, height=45)

for i in sorted(j.split('=')[-1] for j in subprocess.Popen(['adb', 'shell', 'pm list packages -f'], stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n')[:-1]):
    appListbox.insert(tkinter.END, i.split('.')[-1] + '      ' +  i)

appListbox.pack(fill='x')

removeButton = tkinter.Button(root, text="Remove", command=lambda: confirm('Warning', 'This removation could not be undo'))
removeButton.pack(fill='x')

root.mainloop()
